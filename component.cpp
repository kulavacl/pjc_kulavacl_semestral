#ifndef COMPONENT_C
#define COMPONENT_C

#include "component.hpp"

Component::Component(const Node& n){
    m_names.push_back(n.get_name());
    for(auto edge : n.get_egdes()){
        m_edges.push_back(CompEdge(edge.get_v1(), edge.get_v2(), edge.get_cost(), n.get_name()));
    }
    std::sort(m_edges.begin(), m_edges.end(), [](Edge& lhs, Edge& rhs){return lhs > rhs;});
}

void Component::merge(Component& rhs){
    auto left = m_edges.begin();
    auto right = rhs.m_edges.begin();
    auto new_edges = std::vector<CompEdge>();
    while(left != m_edges.end() && right != rhs.m_edges.end()){
        if(*left > *right){
            if(rhs.mergeable(*left)) new_edges.push_back(*left);
            ++left;
        }else{
            if(this->mergeable(*right)) new_edges.push_back(*right);
            ++right;
        }
    }
    if(left == m_edges.end()){
        while(right != rhs.m_edges.end()){
            if(this->mergeable(*right)) new_edges.push_back(*right);
            ++right;
        }
    }else{
        while(left != m_edges.end()){
            if(rhs.mergeable(*left)) new_edges.push_back(*left);
            ++left;
        }
    }
    for(auto node : rhs.m_names){
        m_names.push_back(node);
    }
    for(auto bone : rhs.m_spanning_tree){
        m_spanning_tree.push_back(bone);
    }
    m_edges = std::move(new_edges);
}

bool Component::mergeable(CompEdge& edge){
    for(auto i : m_names){
        if(i == edge.get_outer()) return false;
    }
    return true;
}

bool Component::pop(CompEdge& popped){
    if(m_edges.empty()) return false;
    popped = m_edges.back();
    m_edges.pop_back();
    //m_spanning_tree.push_back(NodeEdge(popped));
    return true;
}

void Component::push_to_spanning_tree(const NodeEdge& bone){
    m_spanning_tree.push_back(bone);
}

bool Component::contains_name(const unsigned int name){
    for(auto m_name : m_names){
        if(m_name == name) return true;
    }
    return false;
}

const std::vector<NodeEdge>& Component::get_spanning_tree() const{
    return m_spanning_tree;
}
const std::vector<unsigned int>& Component::get_names() const{
    return m_names;
}
const std::vector<CompEdge>& Component::get_edges() const{
    return m_edges;
}

std::ostream& Component::show(std::ostream& out) const{
    out<<"spanning tree:\n";
    for(auto edge : m_spanning_tree){
        out<<"    "<<edge;
    }
    out<<"names:\n";
    for(auto name : m_names){
        out<<"    "<<name;
    }
    out<<"\nCompEdges:\n";
    for(auto edge : m_edges){
        out<<"    "<<edge;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, Component& c){
    c.show(out);
    return out;
}

void merge(Component& into, Component& from){
    into.merge(from);
}

#endif