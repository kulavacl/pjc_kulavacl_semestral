#ifndef GRAPH_GEN_H
#define GRAPH_GEN_H
#include <list>
#include <random>
#include <exception>
#include <stdio.h>


#include "graph.hpp"

class impossible_graph_exception : public std::exception{
};
/**
 * @brief raph generatro to demonstrate boruvka solver
 * creates graph that has a spanning tree and some random edges
 * time complexity o(N) - linear with n_edges (the number of nodes does not matter)
 */
class GraphGenerator{
    public:
        GraphGenerator() = default;
        ~GraphGenerator() = default;
        void generate_graph(size_t n_edges, size_t n_nodes, std::vector<Node>& out);
    private:
        std::vector<NodeEdge> create_spanning_tree(size_t n_nodes);
        
};


#endif