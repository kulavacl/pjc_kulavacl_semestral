#ifndef GRAPH_H
#define GRAPH_H
/*
#include "node.cpp"
#include "edge.cpp"
*/
#include "node.hpp"
#include "edge.hpp"

#include <vector>
#include <list>
#include <stdlib.h>
#include <iostream>

class bad_file_format_exception : public std::exception{
    public:
        bad_file_format_exception()=default;
        ~bad_file_format_exception()=default;
        std::string what();
};

class bad_node_exception : public std::exception{
    public:
        bad_node_exception()=default;
        ~bad_node_exception()=default;
        std::string what();
};

class Graph{
    public:
        Graph()=default;
        ~Graph()=default;
        Graph(const std::vector<Node>& in);
        void load_cin_vertical();
        const std::vector<Node>& get_nodes() const;
        void load_from_file(FILE* fp);

    private:
        std::vector<Node> m_nodes;
};

#endif