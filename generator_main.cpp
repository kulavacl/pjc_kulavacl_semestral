#include <stdio.h>
#include <iostream>
#include <time.h>
#include <stdlib.h>

#include "edge.hpp"
#include "node.hpp"
#include "component.hpp"
#include "graph.hpp"
#include "graph_gen.hpp"
#include "generator_switch.hpp"

//#define DEBUG

int main(int argc, char* argv[]){
    SwitchHandler sw = SwitchHandler(argc, argv);
    char *file_out = nullptr;
    int n_edges = 18, n_nodes = 7;
    if(sw.handle(&file_out, n_nodes, n_edges)) return 0;
    Graph g;
    srand(time(nullptr));
    auto gg = GraphGenerator();
    try{
        std::vector<Node> gen_nodes;
        gg.generate_graph(n_edges, n_nodes, gen_nodes);
        g = Graph(gen_nodes);
    }catch (impossible_graph_exception e){
        std::cout<<"Sorry, graph with this few edges can not have a full spanning tree\n";
        return 0;
    }catch (...){
        std::cout<<"Sorry, something weird happend, try again :(\n";
        return 0;
    }
    if(file_out){
        FILE* fp = fopen(file_out, "w");
        if(fp){
            fprintf(fp, "%d\n", n_nodes);
            fprintf(fp, "%d\n", n_edges);
            for (auto& node : g.get_nodes()){
                auto my_name = node.get_name();
                for(auto& edge : node.get_egdes()){
                    if(my_name == edge.get_v1()){
                        fprintf(fp, "%d %d %d\n", edge.get_v1(), edge.get_v2(), edge.get_cost());
                    }
                }
            }
        }else{
            std::cout<<"Sorry, file to save output could not be found or created! :(\n";
            return 103;
        }
        fclose(fp);
    }else{
        printf("%d\n", n_nodes);
        printf("%d\n", n_edges);
        for (auto& node : g.get_nodes()){
            auto my_name = node.get_name();
            for(auto& edge : node.get_egdes()){
                if(my_name == edge.get_v1()){
                    printf("%d %d %d\n", edge.get_v1(), edge.get_v2(), edge.get_cost());
                }
            }
        }
    }
    return 0;
}