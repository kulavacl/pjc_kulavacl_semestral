#ifndef NODE_H
#define NODE_H
/*
#include "edge.cpp"
*/
#include "edge.hpp"
#include <vector>
/**
 * @brief Node object
 * Node object, which serves as a container for name and all edges conainting that name
 */
class Node{
    public:
        Node()=default;
        ~Node()=default;
        Node(std::vector<NodeEdge>& edges, unsigned int name);
        /**
         * @brief Get the name object
         * 
         * @return unsigned int 
         */
        unsigned int get_name() const;
        /**
         * @brief Get the egdes object
         * 
         * @return const std::vector<NodeEdge>& 
         */
        const std::vector<NodeEdge>& get_egdes() const;
    
    private:
        unsigned int m_name;
        std::vector<NodeEdge> m_edges;
};

#endif