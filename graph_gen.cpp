#include "graph_gen.hpp"

std::vector<NodeEdge> GraphGenerator::create_spanning_tree(size_t n_nodes){
    std::vector<NodeEdge> out = std::vector<NodeEdge>();
    std::vector<int> in_spanning_tree = std::vector<int>();
    unsigned int cost = 0;
    unsigned int left = std::rand() % n_nodes;
    unsigned int right = std::rand() % n_nodes;
    while(right == left)
        right = std::rand() % n_nodes;
    in_spanning_tree.push_back(left);
    in_spanning_tree.push_back(right);
    out.push_back(NodeEdge(left, right, ++cost));
    for(unsigned int i = 0; i < n_nodes; i++){
        if(i == left || i == right){
            continue;
        }
        unsigned int neighbour = in_spanning_tree.at(std::rand() % in_spanning_tree.size());
        in_spanning_tree.push_back(i);
        out.push_back(NodeEdge(i, neighbour, ++cost));
    }
    return out;
}

void GraphGenerator::generate_graph(size_t n_edges, size_t n_nodes, std::vector<Node>& out){
    if(n_edges < n_nodes - 1){
        throw impossible_graph_exception();
    }
    std::vector<NodeEdge> to_push = create_spanning_tree(n_nodes);
    unsigned int cost = n_edges;
    for(unsigned int i = 0; i < n_edges - n_nodes + 1; i++){
        try{
            to_push.push_back(NodeEdge(std::rand() % n_nodes, std::rand() % n_nodes, ++cost));
        }catch(bad_edge_exception e){
            i--;
        }
    }
    for(unsigned int i = 0; i < n_nodes; i++){
        out.push_back(Node(to_push, i));
    }
}
