#include <stdio.h>
#include <iostream>
#include <algorithm>

#include <time.h>
/*
#include "edge.cpp"
#include "node.cpp"
#include "component.cpp"
#include "boruvka.cpp"
#include "graph.cpp"
*/
#include "edge.hpp"
#include "node.hpp"
#include "component.hpp"
#include "graph.hpp"
#include "boruvka.hpp"
#include "graph_gen.hpp"
#include "switch.hpp"

//#define DEBUG

bool is_more(const Edge& left, const Edge& right){
    return left > right;
}

int time_diff(struct timespec& start, struct timespec& end, struct timespec& result){
    if(end.tv_nsec < start.tv_nsec && end.tv_sec <= start.tv_sec){
        return -1;
    }
    if(end.tv_nsec < start.tv_nsec){
        --end.tv_sec;
        result.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    }else{
        result.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    result.tv_sec = end.tv_sec - start.tv_sec;

    return 1;
}

int main(int argc, char* argv[]){
    #ifdef DEBUG
        struct timespec start_;
        timespec_get(&start_, TIME_UTC);
        struct timespec end_;
    #endif
    SwitchHandler sw = SwitchHandler(argc, argv);
    char *file_in = nullptr, *file_out = nullptr;
    int n_edges = 18, n_nodes = 7;
    if(sw.handle(&file_in, &file_out, n_nodes, n_edges)) return 0;
    Graph g;
    if(file_in){
        FILE* fp = fopen(file_in, "r");
        if(fp){
            try{
                g.load_from_file(fp);
            }catch (bad_file_format_exception e){
                std::cout<<e.what()<<"For info on format run ./boruvka_example --help\n";
                fclose(fp);
                return 100;
            }catch (bad_node_exception e){
                std::cout<<e.what();
                fclose(fp);
                return 101;
            }catch (...){
                std::cout<<"Something went horribly wrong, sorry :(\n";
                fclose(fp);
                return 2;
            }
        }else{
            std::cout<<"Invalid file given!\n";
            return 102;
        }
        fclose(fp);
    }else{
        srand(time(nullptr));
        auto gg = GraphGenerator();
        try{
            std::vector<Node> gen_nodes;
            gg.generate_graph(n_edges, n_nodes, gen_nodes);
            g = Graph(gen_nodes);
        }catch (impossible_graph_exception e){
            std::cout<<"Sorry, graph with this few edges can not have a full spanning tree\n";
            return 0;
        }catch (...){
            std::cout<<"Sorry, something weird happend, try again :(\n";
            return 0;
        }
    }
    auto bs = BoruvkaSolver(g.get_nodes());
    Component comp;
    struct timespec start;
    struct timespec end;
    timespec_get(&start, TIME_UTC);
    try{
        bs.solve(comp);
    }catch (unsolvable_graph_exception e){
        std::cout<<"This graph does not have a full spanning tree\n";
        return 0;
    }catch (...){
        std::cout<<"Sorry, something went wrong during solving of the graph :(\n";
        return 104;
    }
    timespec_get(&end, TIME_UTC);
    struct timespec result;
    if(time_diff(start, end, result) > 0)
        std::cout<<"solved in "<<result.tv_sec<<"s "<<(result.tv_nsec) / 1000000<<"ms "<<((result.tv_nsec) / 1000) % 1000<<"us "<<result.tv_nsec % 1000<<"ns\n";
    if(file_out){
        FILE* fp = fopen(file_out, "w");
        if(fp){
            fprintf(fp, "%lu\n", comp.get_names().size());
            fprintf(fp, "%lu\n", comp.get_spanning_tree().size());
            for (auto edge : comp.get_spanning_tree()){
                fprintf(fp, "%d %d %d\n", edge.get_v1(), edge.get_v2(), edge.get_cost());
            }
        }else{
            std::cout<<"Sorry, file to save output could not be found or created! :(\n";
            return 103;
        }
        fclose(fp);
    }else{
        std::cout<<comp.get_names().size()<<"\n";
        std::cout<<comp.get_spanning_tree().size()<<"\n";
        for (auto edge : comp.get_spanning_tree()){
            std::cout<<edge.get_v1()<<" "<<edge.get_v2()<<" "<<edge.get_cost()<<"\n";
        }
    }
    #ifdef DEBUG
        timespec_get(&end_, TIME_UTC);
        if(time_diff(start_, end_, result) > 0)
            std::cout<<"overall time "<<result.tv_sec<<"s "<<(result.tv_nsec) / 1000000<<"ms "<<((result.tv_nsec) / 1000) % 1000<<"us "<<result.tv_nsec % 1000<<"ns\n";
    #endif
    return 0;
}