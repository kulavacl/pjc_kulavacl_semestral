#ifndef EDGE_C
#define EDGE_C
#include "edge.hpp"

#define MIN_3(a, b, c) (a) < (b) ? ((a) < (c) ? (a) : (c)) : ((b) < (c) ? (b) : (c))

std::string bad_edge_exception::what(){
    return "Bad edge cought!\n";
}

Edge::Edge(unsigned int v1, unsigned int v2, unsigned int cost){
    if(v1==v2) throw bad_edge_exception(); 
    m_v1 = v1;
    m_v2 = v2;
    m_cost = cost;
}

unsigned int Edge::get_cost() const {
    return m_cost;
}

std::ostream& Edge::show(std::ostream& out) const {
    out<<"edge: "<<m_v1<<" "<<m_v2<<" cost: "<<m_cost<<"\n";
    return out;
}

bool Edge::operator<(const Edge& other){
    return m_cost < other.m_cost;
}

bool Edge::operator>(const Edge& other){
    return m_cost > other.m_cost;
}

Edge& Edge::operator=(const Edge& rhs){
    m_v1 = rhs.m_v1;
    m_v2 = rhs.m_v2;
    m_cost = rhs.m_cost;
    return *this;
}
bool Edge::operator==(const Edge& rhs){
    if(m_cost  != rhs.m_cost) return false;
    if(m_v1 == rhs.m_v2 && m_v2 == rhs.m_v1) return true;
    if(m_v1 == rhs.m_v1 && m_v2 == rhs.m_v2) return true;
    return false;
}

CompEdge::CompEdge(unsigned int v1, unsigned int v2, unsigned int cost, const std::vector<unsigned int>& nodes){
    if(v1==v2) throw bad_edge_exception(); 
    //might add another check for bad edges
    for(unsigned int node : nodes){
        if(node == v1){
            m_v1 = v1;
            m_v2 = v2;
            break;
        }else if(node == v2){
            m_v1 = v2;
            m_v2 = v1;
            break;
        }
    }
    m_cost = cost;
}

CompEdge::CompEdge(unsigned int v1, unsigned int v2, unsigned int cost, const unsigned int name){
    if(v1==v2) throw bad_edge_exception(); 
    //might add another check for bad edges
    if(name == v1){
        m_v1 = v1;
        m_v2 = v2;
    }else if(name == v2){
        m_v1 = v2;
        m_v2 = v1;
    }else{
        throw bad_edge_exception();
    }
    m_cost = cost;
}

unsigned int CompEdge::get_inner() const {
    return m_v1;
}

unsigned int CompEdge::get_outer() const {
    return m_v2;
}

CompEdge& CompEdge::operator=(const CompEdge& rhs){
    m_v1 = rhs.m_v1;
    m_v2 = rhs.m_v2;
    m_cost = rhs.m_cost;
    return *this;
}

NodeEdge::NodeEdge(unsigned int v1, unsigned int v2, unsigned int cost){
    if(v1==v2) throw bad_edge_exception(); 
    if(v1 < v2){
        m_v1 = v1;
        m_v2 = v2;
    }else{
        m_v1 = v2;
        m_v2 = v1;
    }
    m_cost = cost;
}

NodeEdge::NodeEdge(const CompEdge& c_e){
    if(c_e.get_inner() < c_e.get_outer()){
        m_v1 = c_e.get_inner();
        m_v2 = c_e.get_outer();
    }else{
        m_v1 = c_e.get_outer();
        m_v2 = c_e.get_inner();
    }
    m_cost = c_e.get_cost();
}

unsigned int NodeEdge::get_v1() const {
    return m_v1;
}

unsigned int NodeEdge::get_v2()const {
    return m_v2;
}

std::ostream& operator<<(std::ostream& out, Edge& edge){
    edge.show(out);
}

bool operator>(const Edge& left, const Edge& right){
    return left > right;
}

bool operator<(const Edge& left, const Edge& right){
    return left < right;
}

NodeEdge& NodeEdge::operator=(const NodeEdge& rhs){
    m_v1 = rhs.m_v1;
    m_v2 = rhs.m_v2;
    m_cost = rhs.m_cost;
    return *this;
}

bool operator==(const Edge& lhs, const Edge& rhs){
    return lhs == rhs;
}

std::vector<NodeEdge>& edgify(std::vector<unsigned int>& first, std::vector<unsigned int>& second, std::vector<unsigned int>& costs){
    auto out = std::vector<NodeEdge>();
    int failed = 0;
    for(int i = 0; i < MIN_3(first.size(), second.size(), costs.size()); i++){
        try{
            out.push_back(NodeEdge(first[i], second[i], costs[i]));
        }catch (bad_edge_exception e){
            ++failed;
        }
    }
    fprintf(stderr, "failed to parse %d edges\n", failed);
}
#endif