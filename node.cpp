#ifndef NODE_C
#define NODE_C
#include "node.hpp"

Node::Node(std::vector<NodeEdge>& edges, unsigned int name) : m_name(name){
    m_edges = std::vector<NodeEdge>();
    for(auto edge : edges){
        if(edge.get_v1() == m_name || edge.get_v2() == m_name){
            m_edges.push_back(edge);
        }
    }
}

unsigned int Node::get_name() const {
    return m_name;
}

const std::vector<NodeEdge>& Node::get_egdes() const {
    return m_edges;
}

#endif