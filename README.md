Semestrálka obsahuje implementaci Borůvkova algoritmu pro jedno vlákno. Ta je v souboru boruvka_solver, který lze vytvořit přidaným CMakeLists.txt.
Solver načte data grafu, každému vrcholu vytvoří komponentu, ty potom postupně spojuje nejlevnější hranou. Předpokládá, že žádné 2 hrany nemají stejnou cenu. Když zbývá jen jedna komponenta, je v její kostře řešení. Pokud některá z komponent nemá hranu, kterou by šlo přidat do kostry, graf nemá kostru a program vypíš chybu a skončí.
Implementace je jen pro jedno vlákno, pro více vláken byla pomalejší, tak zde není.
Druhý soubor je graph_generator, který generuje graph, který vždy má nějakou kostru. Je přiložený pro generaci grafů pro solver.
Přiložené grafy jsou malá data (graph1.in), neřešitelný graf (graph2.in) a velký graf (graph3.in) - řešení velkého grafu trvá zhruba 30 minut.

Zpráva vytvořená proti commitu hash: 2bca2225371adbb14ae16862f585f09e2eed136c.
