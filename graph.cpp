#ifndef GRAPH_C
#define GRAPH_C
#include "graph.hpp"

std::string bad_file_format_exception::what(){
    return "The file provided is not in specified format!\n";
}

std::string bad_node_exception::what(){
    return "Node value is higher than n_nodes - 1!\n";
}

void Graph::load_cin_vertical(){
    unsigned int v1,v2,cost;
    unsigned int n_edges, n_nodes;
    std::cin>>n_nodes;
    std::cin>>n_edges;
    auto edges = std::vector<NodeEdge>();
    auto vals = std::vector<unsigned int>();
    for(int i = 0; i < n_edges; i++){
        std::cin>>v1;
        std::cin>>v2;
        std::cin>>cost;
        vals.push_back(v1);
        vals.push_back(v2);
        vals.push_back(cost);
    }
    for(int i = 0; i < vals.size(); i+=3){
        try{
            auto my_edge = NodeEdge(vals.at(i), vals.at(i+1), vals.at(i+2));
            edges.push_back(my_edge);
        }catch (bad_edge_exception e){
            ;
        }
    }
    for(int i = 0; i < n_nodes; i++){
        m_nodes.push_back(Node(edges, i));
    }
}

Graph::Graph(const std::vector<Node>& in){
    m_nodes = in;
}

void Graph::load_from_file(FILE* fp){
    unsigned int v1,v2,cost;
    unsigned int n_edges, n_nodes;
    if(fscanf(fp, "%d", &n_nodes) != 1) throw bad_file_format_exception();
    if(fscanf(fp, "%d", &n_edges) != 1) throw bad_file_format_exception();
    auto edges = std::vector<NodeEdge>();
    auto vals = std::vector<unsigned int>();
    for(int i = 0; i < n_edges; i++){
        if(fscanf(fp, "%d", &v1) != 1) throw bad_file_format_exception();
        if(fscanf(fp, "%d", &v2) != 1) throw bad_file_format_exception();
        if(fscanf(fp, "%d", &cost) != 1) throw bad_file_format_exception();
        if(v1 >= n_nodes || v2 >= n_nodes) throw bad_node_exception();
        vals.push_back(v1);
        vals.push_back(v2);
        vals.push_back(cost);
    }
    for(int i = 0; i < vals.size(); i+=3){
        try{
            auto my_edge = NodeEdge(vals.at(i), vals.at(i+1), vals.at(i+2));
            edges.push_back(my_edge);
        }catch (bad_edge_exception e){
            ;
        }
    }
    for(int i = 0; i < n_nodes; i++){
        m_nodes.push_back(Node(edges, i));
    }
}

const std::vector<Node>& Graph::get_nodes() const {
    return m_nodes;
}

#endif