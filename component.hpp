#ifndef COMPONENT_H
#define COMPONENT_H
/*
#include "edge.cpp"
#include "node.cpp"
*/
#include "edge.hpp"
#include "node.hpp"

#include <mutex>
#include <algorithm>

class Component{
    public:
        Component()=default;
        ~Component()=default;
        Component(const Node& n);
        std::ostream& show(std::ostream& out) const;
        /**
         * @brief merges component to this component
         * Merges component to this compomnent, after merge:
         * rhs is the same as before
         * this now contains all names and edges from this and rhs
         * @param rhs component to merge with this
         */
        void merge(Component& rhs);
        /**
         * @brief Get the spanning tree object
         * 
         * @return const std::vector<NodeEdge>& 
         */
        const std::vector<NodeEdge>& get_spanning_tree() const;
        /**
         * @brief Get the names object
         * 
         * @return const std::vector<unsigned int>& 
         */
        const std::vector<unsigned int>& get_names() const;
        /**
         * @brief Get the edges object
         * 
         * @return const std::vector<CompEdge>& 
         */
        const std::vector<CompEdge>& get_edges() const;
        /**
         * @brief pop the cheapest edge
         * finds the cheapest edge of this component and saves it to popped
         * @param popped CompEdge to save the cheapest edge in
         * @return true edge was found and saved to popped
         * @return false there are no edges in this component, graph cennot be solved
         */
        bool pop(CompEdge& popped);
        /**
         * @brief true if a name of node is present, false otherwise
         * 
         * @param name name of node to be found
         * @return true node is present
         * @return false node is not present
         */
        bool contains_name(const unsigned int name);
        /**
         * @brief pushes NodeEdge bone to spanning tree
         * 
         * @param bone edge to be pushed
         */
        void push_to_spanning_tree(const NodeEdge& bone);

    private:
        /**
         * @brief true if CompEdge edge can be added
         * Find if CompEdge edge can be added ie. if the edge spans
         * out of this component (if it ends in this compnent, merging it would create
         * an inside compomnent edge)
         * @param edge edge to check
         * @return true edge can be merged into this
         * @return false edge cannot be merged into this
         */
        bool mergeable(CompEdge& edge);
        std::vector<NodeEdge> m_spanning_tree;
        std::vector<unsigned int> m_names;
        std::vector<CompEdge> m_edges;
};

std::ostream& operator<<(std::ostream& out, Component c);

/**
 * @brief merges two components
 * merges compnent from into compnent into:
 * after merge, into contains all edges from into and from
 * all names from into and from and
 * full spanning tree of into and from
 * from is kept the same after the merge
 * @param into Component which becomes the merged one
 * @param from Component which is not changed
 */
void merge(Component& into, Component& from);

#endif