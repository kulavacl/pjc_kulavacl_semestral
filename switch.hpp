#ifndef SWITCH_H
#define SWITCH_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#define HELP_TEXT "boruvka_solver\n\
Implements Boruvka's algorithm to solve given graph.\nArguments:\n\
\t--help, --Help, --h, --H: prints this text.\n\
\t-FI <filename>, -fi <filename>: file in, loads graph from filename, required format:\n\
\t\tn_nodes\n\
\t\tn_edges\n\
\t\tfrom1 to1 cost1\n\
\t\t.\n\
\t\t.\n\
\t\t.\n\
\t\tfromN toN costN\n\
\t-FO <filename> -fo <filename>: file out, saves spanning tree into filename, same format as file in\n\
\t-r <n_nodes> <n_edges>, -R <n_nodes> <n_edges>: randomly generates graph and finds its minimal spanning tree. If both -R and -FI are used, -FI takes priority\n"

class SwitchHandler{
    public:
        SwitchHandler()=default;
        ~SwitchHandler()=default;
        SwitchHandler(int& in_argc, char** in_argv);
        bool handle(char** file_in, char** file_out, int& n_nodes, int& n_edges);

    private:
        bool is_help(char* arg);
        bool is_file_in(char* arg);
        bool is_file_out(char* arg);
        bool is_random_gen(char* arg);
        bool is_number(char* arg);
        int m_argc;
        char** m_argv;
};

#endif