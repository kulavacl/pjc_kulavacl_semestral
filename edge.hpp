#ifndef EDGE_H
#define EDGE_H

#include <vector>
#include <queue>
#include <exception>
#include <string>
#include <iostream>

class bad_edge_exception : public std::exception{
    public:
        bad_edge_exception()=default;
        ~bad_edge_exception()=default;
        std::string what();
};
/**
 * @brief only parent class
 * parent class serving as baseline for CompEdge and NodeEdge, 
 * which provide different get methods with different guarantees
 */
class Edge{
    public:
        Edge()=default;
        Edge(unsigned int v1, unsigned int v2, unsigned int cost);
        ~Edge()=default;
        /**
         * @brief Get the cost object
         * 
         * @return unsigned int 
         */
        unsigned int get_cost() const;
        std::ostream& show(std::ostream& out) const;
        bool operator<(const Edge& other);
        bool operator>(const Edge& other);
        bool operator==(const Edge& rhs);
        Edge& operator=(const Edge& rhs);
        
    
    protected:
        unsigned int m_v1;
        unsigned int m_v2;
        unsigned int m_cost;
};
/**
 * @brief used in Component to represent edges
 * get_inner and get_outer differentiate between nodes present and not present in Component
 */
class CompEdge : public Edge{
    public:
        CompEdge()=default;
        CompEdge(unsigned int v1, unsigned int v2, unsigned int cost, const std::vector<unsigned int>& nodes);
        CompEdge(unsigned int v1, unsigned int v2, unsigned int cost, const unsigned int name);
        ~CompEdge()=default;
        CompEdge& operator=(const CompEdge& rhs);
        /**
         * @brief Get the inner object
         * inner is always present in comp
         * @return unsigned int 
         */
        unsigned int get_inner() const;
        /**
         * @brief Get the outer object
         * outer is never present in comp
         * @return unsigned int 
         */
        unsigned int get_outer() const;
};

/**
 * @brief more tha Edge but less than CompEdge
 * created for nodes before CompEdge can be created (CompEdge requires
 * components to exist and Compnents require Nodes and Nodes require NodeEdges)
 */
class NodeEdge : public Edge{
    public:
        NodeEdge()=default;
        NodeEdge(unsigned int v1, unsigned int v2, unsigned int cost);
        NodeEdge(const CompEdge& c_e);
        ~NodeEdge()=default;
        /**
         * @brief Get the v1 object
         * v1 is always lesser than v2
         * @return unsigned int 
         */
        unsigned int get_v1() const;
        /**
         * @brief Get the v2 object
         * v2 is always lesser than v1
         * @return unsigned int 
         */
        unsigned int get_v2() const;
        NodeEdge& operator=(const NodeEdge& rhs);
};

std::ostream& operator<<(std::ostream& out, Edge& edge);

bool operator>(const Edge& left, const Edge& right);

bool operator<(const Edge& left, const Edge& right);

bool operator==(const Edge& lhs, const Edge& rhs);

/**
 * @brief make vector of nodes from params
 * takes edges specified as edge[i] = first[i], second[i], costs[i] and returns a vector of said edges
 * discarding unreasonable edges (edges starting and ending in the same node)
 * and also discarding all values larger than shortest of params
 * 
 * @param first first[i] = starting node of edge[i]
 * @param second second[i] = ending point of edge[i]
 * @param costs costs[i] = cost of edge[i]
 * @return std::vector<Node>& reference to vector of edges
 */
std::vector<NodeEdge>& edgify(std::vector<unsigned int>& first, std::vector<unsigned int>& second, std::vector<unsigned int>& costs);

#endif