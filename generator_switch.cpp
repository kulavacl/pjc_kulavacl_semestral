#ifndef SWITCH_C
#define SWITCH_C

#include "generator_switch.hpp"

SwitchHandler::SwitchHandler(int& in_argc, char** in_argv){
    m_argc = in_argc;
    m_argv = in_argv;
}

bool SwitchHandler::handle(char** file_out, int& n_nodes, int& n_edges){
    unsigned current = 1;
    while(current < m_argc){
        if(is_help(m_argv[current])){
            std::cout<<HELP_TEXT;
            return true;
        }else if(is_file_out(m_argv[current])){
            if(++current >= m_argc){
                std::cout<<"missing argument for switch -FO\n";
                return true;
            }
            *file_out = m_argv[current];
        }else if(is_random_gen(m_argv[current])){
            if(++current >= m_argc){
                std::cout<<"missing argument for switch -R\n";
                return true;
            }
            if(!is_number(m_argv[current])){
                std::cout<<"rangom generator can't make a number out of "<<m_argv[current]<<"\n";
                return true;
            }
            n_nodes = atoi(m_argv[current]);
            if(++current >= m_argc){
                std::cout<<"missing argument for switch -R\n";
                return true;
            }
            if(!is_number(m_argv[current])){
                std::cout<<"rangom generator can't make a number out of "<<m_argv[current]<<"\n";
                return true;
            }
            n_edges = atoi(m_argv[current]);
        }else{
            std::cout<<"unknown argument: "<<m_argv[current]<<"\n";
        }
        current++;
    }
    return false;
}

bool SwitchHandler::is_help(char* arg){
    int current = 0;
    if(arg[current] != '-') return false;
    if(!arg[++current]) return false;
    if(arg[current] != '-') return false;
    if(!arg[++current]) return false;
    if(arg[current] != 'H' && arg[current] != 'h') return false;
    if(!arg[++current]) return true;
    if(arg[current] != 'e') return false;
    if(!arg[++current]) return false;
    if(arg[current] != 'l') return false;
    if(!arg[++current]) return false;
    if(arg[current] != 'p') return false;
    if(!arg[++current]) return true;
    return false;
}

bool SwitchHandler::is_file_out(char* arg){
    if(arg[0] != '-') return false;
    if(arg[1] == 'F' && arg[2] == 'O' && !arg[3]) return true;
    if(arg[1] == 'f' && arg[2] == 'o' && !arg[3]) return true;
    return false;
    return false;
}

bool SwitchHandler::is_random_gen(char* arg){
    if(arg[0] == '-' && (arg[1] == 'r' || arg[1] == 'R') && !arg[2]) return true;
    return false;
}

bool SwitchHandler::is_number(char* arg){
    int current = 0;
    while(arg[current]){
        if(arg[current] < '0' || arg[current] > '9') return false;
        ++current;
    }
    return true;
}

#endif