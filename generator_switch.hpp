#ifndef SWITCH_H
#define SWITCH_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#define HELP_TEXT "graph_generator\n\
Generates random graph and prints it to stdout or to given file.\nArguments:\n\
\t--help, --Help, --h, --H: prints this text.\n\
\t-FO <filename> -fo <filename>: file out, saves generated graph into filename, format:\n\
\t\tn_nodes\n\
\t\tn_edges\n\
\t\tfrom1 to1 cost1\n\
\t\t.\n\
\t\t.\n\
\t\t.\n\
\t\tfromN toN costN\n\
\t-r <n_nodes> <n_edges>, -R <n_nodes> <n_edges>: randomly generates graph with n_nodes and n_edges\n"

class SwitchHandler{
    public:
        SwitchHandler()=default;
        ~SwitchHandler()=default;
        SwitchHandler(int& in_argc, char** in_argv);
        bool handle(char** file_out, int& n_nodes, int& n_edges);

    private:
        bool is_help(char* arg);
        bool is_file_out(char* arg);
        bool is_random_gen(char* arg);
        bool is_number(char* arg);
        int m_argc;
        char** m_argv;
};

#endif