#ifndef BORUVKA_C
#define BORUVKA_C

#include "boruvka.hpp"

std::string unsolvable_graph_exception::what(){
    return "Graph has more than one component!\n";
}

BoruvkaSolver::BoruvkaSolver(const std::vector<Node>& nodes){
    m_components = std::list<Component>();
    for(auto& node : nodes){
        m_components.push_back(Component(node));
    }
}

bool BoruvkaSolver::solve(Component& result){
    //when there is only one component, it conatins the spanning tree
    while(m_components.size() > 1){
        //find edges to push to the spanning tree
        auto to_process = std::vector<CompEdge>();
        for(auto& comp : m_components){
            auto popped_edge = CompEdge();
            if(!comp.pop(popped_edge)){
                throw unsolvable_graph_exception();
            }
            if(std::find(to_process.begin(), to_process.end(), popped_edge) == to_process.end()){
                comp.push_to_spanning_tree(NodeEdge(popped_edge));
                to_process.push_back(popped_edge);
            }
        }
        //merge components, being left with half the number of them
        for(auto& edge : to_process){
            auto two = m_components.begin();
            auto one = m_components.begin();
            while(!one->contains_name(edge.get_inner())){
                ++one;
            }
            while(!two->contains_name(edge.get_outer())){
                ++two;
            }
            merge(*one, *two);
            m_components.erase(two);
        }
    }
    result = m_components.front();
    return true;
}

const std::list<Component>& BoruvkaSolver::get_components() const{
    return m_components;
}

#endif