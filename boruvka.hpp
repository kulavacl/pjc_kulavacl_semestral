#ifndef BORUVKA_H
#define BORUVKA_H
/*
#include "edge.cpp"
#include "node.cpp"
#include "component.cpp"
*/
#include "edge.hpp"
#include "node.hpp"
#include "component.hpp"

#include <thread>
#include <semaphore.h>
#include <list>
#include <vector>
#include <exception>
#include <functional>

class unsolvable_graph_exception : public std::exception{
    std::string what();
};
/**
 * @brief solver to find the cheapest spanning tree of graph
 * usese Boruvka's algorithm to find the lowest spanning tree of graph,
 * requires graph in which each edge has a different cost.
 * If there is more than one edge with the same cost, the result is
 * deterministic, but there are no guarantees - it can be optimal, 
 * suboptimal or not a spanning tree at all.
 */
class BoruvkaSolver{
    public:
        BoruvkaSolver()=default;
        ~BoruvkaSolver()=default;
        BoruvkaSolver(const std::vector<Node>& nodes);
        const std::list<Component>& get_components() const;
        /**
         * @brief find the chapest spanning tree
         * find the cheapest spanning tree
         * throws unsolable
         * @param result Component to save the solution in
         * @return true solution was found
         * @return false solution was not found
         */
        bool solve(Component& result);

    private:
        std::list<Component> m_components;
};

#endif